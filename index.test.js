const alexify = require("./");

// basic stuff
test("format messages properly", () => {
  expect(alexify("a", "b")).toMatch(/ready to A some B/);
});

// localization
test("respect locale", () => {
  expect(alexify("iç", "çay", "tr")).toMatch(/^ready to İÇ some ÇAY$/);
});

// defaults
test("have sensible defaults", () => {
  expect(alexify()).toMatch(/ready to PARSE some VAST/);
});

test("have sensible defaults, one argument only", () => {
  expect(alexify("test")).toMatch(/^ready to TEST some VAST$/);
});

// error checking
test("throw on invalid type for verb", () => {
  expect(() => alexify(5)).toThrow(/^Expected verb as string$/);
});

test("throw on invalid type for object", () => {
  expect(() => alexify("test", 5)).toThrow(/^Expected object as string$/);
});
