const Default = {
  verb: "parse",
  object: "vast"
};

module.exports = (verb = Default.verb, object = Default.object, locale) => {
  if (!verb || typeof verb !== "string") {
    throw new Error("Expected verb as string");
  }

  if (!object || typeof object !== "string") {
    throw new Error("Expected object as string");
  }

  const upperCase = string => string.toLocaleUpperCase(locale);
  return `ready to ${upperCase(verb)} some ${upperCase(object)}`;
};
